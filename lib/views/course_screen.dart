import 'package:flutter/material.dart';

class CourseScreen extends StatelessWidget {
  List<String> courses;

  CourseScreen(this.courses, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Enrolled courses')),
      body: ListView.builder(
        itemCount: courses.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(courses[index]),
          );
        },
      ),
    );
  }
}
