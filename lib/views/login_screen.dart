import 'package:flutter/material.dart';
import 'dart:convert';
import '../controllers/sakai_controller.dart';
import '../services/sakai_services.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'course_screen.dart';


class LoginScreen extends StatefulWidget {
  static const route = '/login';
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }

}

class LoginScreenState extends State<StatefulWidget>{
  final formKey = GlobalKey<FormState>();
  final SakaiController _controller = SakaiController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final serverController = TextEditingController()..text = 'https://xlms.myworkspace.vn';
  late String emailAddress;
  late String password;
  late String server;
  String authMessage = '';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Login'),),
      body: buildLoginForm(),
    );
  }

  Widget buildLoginForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          Align(alignment: Alignment.centerLeft, child: new Text("Username",textScaleFactor: 1.75 ),),
          emailField(),
          Align(alignment: Alignment.centerLeft, child: new Text("Password",textScaleFactor: 1.75),),
          passwordField(),
          Align(alignment: Alignment.centerLeft, child: new Text("Server",textScaleFactor: 1.75 ),),
          serverField(),
          loginButton()
        ],
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      controller: emailController,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return 'Pls input valid email.';
        }
        return null;
      },
      onSaved: (value) {
        emailAddress = value as String;
      },

    );
  }

  Widget passwordField() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),
      validator:(value) {
        if (value!.length < 8) {
          return 'Password must be at least 8 characters.';
        }
      },
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget serverField() {
    return TextFormField(
      controller: serverController,
      decoration: InputDecoration(
          icon: Icon(Icons.insert_link),
      ),
      validator:(value) {
        if (value!= 'https://xlms.myworkspace.vn') {
          serverController..text = value!;
        }
      },
      onSaved: (value) {
        server = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () async {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('$emailAddress, $password, $server');

            dynamic authResponse =
                await _controller.authenticate(emailAddress, password);
            print(authResponse.statusCode);

            List<String> enrolledCourses = await _controller.getSites();
            print(enrolledCourses);
            if (authResponse.statusCode == 201 ||
                authResponse.statusCode == 200) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CourseScreen(enrolledCourses)));
              authMessage = '';
              setState(() {});
            } else {
              authMessage = 'Authentication failed.';
              setState(() {});
            }
          }
        },
      child: const Text('Login'),
    );
  }


}